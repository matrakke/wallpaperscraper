# pyqt5

**Requires python < 3.11**

The wallpapers are downloaded from:

            https://wallpaperscraft.com

On this site the wallpapers on different
pages are grouped by 15. That's why
the steps are by 15 to choose how many wallpapers you want.

The downloaded images are stored in the choosen folder or you can use the default.

The images will be stored in a subfolder with the name of the category you choose.

After the progressbar show Done!, you can just download other categories.

Terryn Serge aka Essetee


GNU GENERAL PUBLIC LICENSE
           Version 3, 29 June 2007


The best way to execute the code is inside a virtual environment.

Ubuntu: sudo apt get install python3-pip python3-virtualenv python3-virtualenvwrapper python3-venv
Fedora: sudo dnf install python-pip-wheel python3-virtualenv  python3-virtualenvwrapper python3-pytest-venv

Activate your virtualenv (python3 -m venv venv | windows: py -m venv venv)
then source venv/bin/activate

Windows: ./venv/Script/activate.bat

When you are in your virtualenv, run pip install -r requirements.txt

Then you can right click the main.py file and choose to run it. (PyCharm)

If you use vscode, just click on te main.py file and then in the right upper corner click on the run symbol.

Cheers


