from cProfile import label
from re import M
import sys
import form
from PyQt5.QtCore import QStandardPaths, QDir
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox, QDesktopWidget, QFileDialog, QProgressBar
from bs4 import BeautifulSoup as BS
import threading
import os
import requests
import ntpath



class Window(QMainWindow, form.Ui_MainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.center(self)
        self.setConfigDir()
        saveDir = QStandardPaths.locate(QStandardPaths.GenericDataLocation, str(), QStandardPaths.LocateDirectory) + "wallpapers/"
        self.labelPadNieuw.setText(saveDir)
        self.pushButton.clicked.connect(self.kiespad)
        self.pushButtonGetWallpapers.clicked.connect(self.makeConfig)
        self.downloaded_files = 0
        self.total_files = 0
        self.comboBoxResolution.setCurrentIndex(11)
        #self.actionAbout.triggered.connect(self.showdialog)
        

    def linkConverter(self, link,resolution):
        return link.replace("300x168",resolution)

    def getData(self, url):
        r = requests.get(url)
        if(r.status_code == 200):
            return r.text

    def download(self,link,foldername):
        self.downloaded_files, self.total_files
        self.pBar.setMinimum(0)
        self.pBar.setMaximum(self.spinBoxHoeveelWallpapers.value())
        name = self.getFileName(link)
        r = requests.get(link)
        foldertest = foldername
        if not (os.path.isdir(foldertest)):
            try:  
                os.mkdir(foldertest)
            except OSError:  
                print ("Creation of the directory "+foldername+" failed")
        with open(foldername+"/"+name,'wb') as f: 
            f.write(r.content)
            self.downloaded_files+=1
            self.pBar.setValue(self.downloaded_files)
            print('self.downloaded_files: ', self.downloaded_files)
        if self.downloaded_files == self.spinBoxHoeveelWallpapers.value():
            self.pBar.setFormat('Done !')

    def getFileName(self, link):
        return ntpath.basename(link)

    def getNumberOfWallpapers(self):
        want = self.spinBoxHoeveelWallpapers.value()
        give = int(want/15)
        return '1-' + str(give)

    def downloadAllFromPageLink(self, link,resolution,foldername,tf):
        self.total_files = tf
        soup = BS(self.getData(link), 'html.parser')
        allWalls = soup.findAll("img", {"class": "wallpapers__image"})
        for wall in allWalls:
            mainLink = self.linkConverter(wall['src'],resolution)
            self.download(mainLink,foldername)


    def makeConfig(self):
        mainLink = "https://wallpaperscraft.com/catalog/" + self.findCategory().lower()
        if mainLink == "https://wallpaperscraft.com/catalog/technologies":
            mainLink = "https://wallpaperscraft.com/catalog/hi-tech"
        threads = 2
        resolution = self.findResolution()
        foldername = self.labelPadNieuw.text()
        pages_to_download = self.getNumberOfWallpapers()
        
        soup = BS(self.getData(mainLink), 'html.parser')

        # trying to get the first and last page
        if(len(pages_to_download.split('-')) < 2):
            firstpage = 1
            lastpage = int(self.getFileName(soup.findAll("a", {"class":"pager__link"})[2]['href']).replace("page",""))
        else:
            firstpage = int(pages_to_download.split('-')[0])
            lastpage = int(pages_to_download.split('-')[1])

        # looping through all the page to download wallpapers with multithreading
        all_threads = []
        for i in range(firstpage,lastpage+1):
            download_thread = threading.Thread(target=self.downloadAllFromPageLink, args=(mainLink+"/page"+str(i),resolution,foldername,lastpage*15,))
            download_thread.start()
            all_threads.append(download_thread)
            if(len(all_threads)>threads-1):
                for t in all_threads:
                    t.join()
            all_threads = []

    def findCategory(self):
        return self.comboBoxCategory.currentText()

    def findResolution(self):
        return self.comboBoxResolution.currentText()

    def kiespad(self):
        file = str(QFileDialog.getExistingDirectory(self, "Select Directory"))
        saveDir = file + '/' + self.findCategory() + '/'
        myDir = QDir()
        if not myDir.exists(saveDir):
            myDir.mkpath(saveDir)
        self.labelPadNieuw.setText(saveDir)

    @staticmethod
    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    @staticmethod
    def errorMsg(self, errormsg):
        msg = QMessageBox()
        msg.setWindowTitle("Error!")
        msg.setText(errormsg)
        msg.exec()

    @staticmethod
    def setConfigDir():
        saveDir = QStandardPaths.locate(QStandardPaths.GenericDataLocation, str(), QStandardPaths.LocateDirectory) + "wallpapers/"
        myDir = QDir()
        if not myDir.exists(saveDir):
            myDir.mkpath(saveDir)
        return saveDir

if __name__ == "__main__":
    app = QApplication(sys.argv)
    win = Window()
    win.show()
    sys.exit(app.exec())
